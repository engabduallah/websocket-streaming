python websockets_server.py --hostname "localhost" --port 8766 --server_files_dir "server-files"
python tornado_server.py --hostname "localhost" --port 8765 --server_files_dir "server-files"
python clients-analysis.py --client_counts 1 10 20 50 --modes data file --client_files_path  "client-files/" --metric_plots_path "metric_plots/" --metric_results_path "metric_results/" --tornado_hostname "localhost:8765" --websockets_hostname "localhost:8765"



