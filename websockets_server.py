"""
Author: Abdullah Damash
Date: 15/12/2023
Copyright (c) 2023, A.Damash. All rights reserved.

This code is written by A.Damash following the honor codes.
"""

import asyncio
import websockets
import json
import argparse
from message_handler import MessageHandler


class WebSocketServer:
    def __init__(self, port, server_files_dir, hostname):
        """
        Initialize the WebSocketServer with a specified port and directory for server files.
        """
        self.app = None
        self.hostname = hostname
        self.port = port
        self.server_files_dir = server_files_dir

    async def server(self, websocket):
        """
        Asynchronously handle incoming messages on the WebSocket. This coroutine is called
        for each client connection.
        """
        async for message in websocket:
            try:
                # Parse the JSON message received from the client
                parsed_message = json.loads(message)
                if parsed_message.get("mode") == "close connection":
                    print("Close command received. Closing connection.")
                    await self.stop_websockets_server()
                    break
                    # Initialize the MessageHandler with the parsed message and callback functions
                handler = MessageHandler(parsed_message, lambda msg, binary=False: websocket.send(msg),
                                         self.server_files_dir)

                # Respond to the message
                await handler.respond()
            except Exception as e:
                # Log an error message if there's an issue with the WebSocket connection
                print(f"Error in WebSocket connection: {e}")

    async def start_websockets_server(self):
        """
        Start the WebSocket server. It listens on the specified port and handles
        incoming connections.
        """
        # Start the WebSocket server and store the server object
        self.app = await websockets.serve(self.server, self.hostname, self.port)
        await asyncio.Future()  # Keep the server running indefinitely

    async def stop_websockets_server(self):
        """
        Stop the WebSocket server based on the mode: close connection sent by client.
        """
        if self.app is not None:
            self.app.close()
            self.app.wait_closed()
            asyncio.get_event_loop().stop()
            print("server has been closed")
            exit(0)
        else:
            print("Server did not start")


if __name__ == "__main__":
    # Create an argument parser for command-line execution
    parser = argparse.ArgumentParser(description='Start the WebSocket server with specified port and directory.')

    # Define the expected command-line arguments
    parser.add_argument('--hostname', type=str, help='hostname of the server.')
    parser.add_argument('--port', type=int, help='Port number for the Tornado server to listen on.')
    parser.add_argument('--server_files_dir', type=str, help='Directory path for the server files.')

    # Parse the arguments passed to the command line
    args = parser.parse_args()

    # Instantiate and run the WebSocket server with the provided arguments
    websockets_server = WebSocketServer(args.port, args.server_files_dir, args.hostname)
    asyncio.run(websockets_server.start_websockets_server())
