import argparse
import json
import os
import asyncio
import websockets
import time
import psutil
import pandas as pd
import matplotlib.pyplot as plt
from tornado.ioloop import IOLoop


async def test_client(uri, all_mode, client_files_dir,close_flag, duration=10):
    """
    Asynchronously tests a WebSocket client for a specified duration, sending messages
    to the server and measuring response times.
    """
    async with websockets.connect(uri) as websocket:
        start_time = time.time()
        message_count = 0
        total_latency = 0

        while time.time() - start_time < duration:
            send_time = time.time()
            if close_flag:
                await websocket.send(json.dumps({"mode": "close connection"}))
                break
            else:
                await websocket.send(json.dumps({"mode": all_mode, "time": send_time}))
            response = await websocket.recv()
            handle_response(response, client_files_dir, int(time.time()), all_mode)
            receive_time = time.time()

            latency = receive_time - send_time
            total_latency += latency
            message_count += 1

        return total_latency, message_count


def handle_response(response, file_path, receive_time, mode):
    """
    Handles the response received from the server. It processes and saves the response
    based on the mode (data or file).
    """
    if not response:
        print("Received an empty response.")
        return

    try:
        if mode == "data":
            if isinstance(response, bytes):
                response = response.decode('utf-8')
            json_data = json.loads(response)
            with open(os.path.join(file_path, f'{receive_time}_data.json'), 'w') as file:
                json.dump(json_data, file)
        elif mode == "file":
            if isinstance(response, str):
                response = response.encode()
            file_path = os.path.join(file_path, f'received_file_{receive_time}')
            with open(file_path, 'wb') as file:
                file.write(response)
    except Exception as e:
        print(f"Error processing response: {e}\nResponse: {response}")


async def run_test(uri, client_count, all_mode, client_files_dir, close_flag):
    """
    Runs the test for a set number of clients, calculating metrics such as latency,
    throughput, CPU and memory usage.
    """
    result = {}
    for count in client_count:
        if close_flag:
            await test_client(uri, all_mode, client_files_dir, close_flag)
        else:
            tasks = [test_client(uri, all_mode, client_files_dir, close_flag) for _ in range(count)]
            gathered_results = await asyncio.gather(*tasks)
            total_latency = sum(latency for latency, _ in gathered_results)
            total_messages = sum(messages for _, messages in gathered_results)
            average_latency = total_latency / total_messages if total_messages else float('inf')
            throughput = total_messages / (10 * count)  # Duration * count

            cpu_usage = psutil.cpu_percent()
            memory_usage = psutil.virtual_memory().percent

            print(f"Test with {count} clients in '{all_mode}' mode:")
            print(f"Average Latency: {average_latency} seconds")
            print(f"Throughput: {throughput} messages/second per client")
            print(f"CPU Usage: {cpu_usage}%")
            print(f"Memory Usage: {memory_usage}%")

            result[count] = (average_latency, throughput, cpu_usage, memory_usage)
    return result


async def run_tests(uri, client_count, all_mode, client_files_dir, close_flag):
    """
    Executes the run_test function for different modes (data, file etc.)
    and aggregates the results.
    """
    result = {}
    for one_mode in all_mode:
        result[one_mode] = await run_test(uri, client_count, one_mode, client_files_dir, close_flag)
    return result


def tornado_test(hostname, all_mode, client_files_dir, client_count, close_flag=False):
    """
    Tests the Tornado WebSocket server by running the client test suite.
    """
    tornado_uri = f"ws://{hostname}"
    result = {'Tornado': asyncio.run(run_tests(tornado_uri, client_count, all_mode, client_files_dir, close_flag))}
    IOLoop.current().stop()
    return result


async def websockets_test(hostname, all_mode, client_files_dir, client_count, close_flag=False):
    """
    Tests the websockets server implementation by running the client test suite.
    """
    websockets_uri = f"ws://{hostname}"
    return {'websockets': await run_tests(websockets_uri, client_count, all_mode, client_files_dir, close_flag)}


def plot_and_save_metric(results, metric_index, title, y_label, metric_plots_dir):
    """
    Plots and saves the specified metric for each server type and mode.
    """
    for server_type in results:
        for mode in results[server_type]:
            plt.figure(figsize=(10, 6))
            x_values = list(results[server_type][mode].keys())
            y_values = [results[server_type][mode][x][metric_index] for x in x_values]

            plt.plot(x_values, y_values, marker='o')
            plt.title(f'{title} ({mode} Mode)')
            plt.xlabel('Number of Clients')
            plt.ylabel(y_label)
            plt.grid(True)
            plt.savefig(os.path.join(metric_plots_dir, f'{server_type}_{mode}_{title.replace(" ", "_").lower()}.png'))
            plt.close()


def save_final_bar_chart(results, metric_plots_dir):
    """
    Saves a final bar chart comparing different metrics across server types and modes.
    """
    metrics = ['Average Latency', 'Throughput', 'CPU Usage', 'Memory Usage']
    plt.figure(figsize=(10, 6))

    for i, metric in enumerate(metrics):
        plt.subplot(2, 2, i + 1)
        for server_type in results:
            for mode in results[server_type]:
                x = [f'{server_type} - {mode}']
                print(results[server_type][mode][1])
                y = [results[server_type][mode][1][i]]
                plt.bar(x, y, label=f'{server_type} - {mode}')

        plt.ylabel(metric)
        plt.xticks(rotation=45)
        plt.legend()

    plt.tight_layout()
    plt.savefig(os.path.join(metric_plots_dir, 'final_results_bar_chart.png'))
    plt.close()


def save_final_results(results, metric_results_dir):
    """
    Saves the final results as a CSV file.
    """
    data = []
    for server_type in results:
        for mode in results[server_type]:
            for client_count, metrics in results[server_type][mode].items():
                latency, throughput, cpu_usage, memory_usage = metrics
                data.append([server_type, mode, client_count, latency, throughput, cpu_usage, memory_usage])

    df = pd.DataFrame(data, columns=['Server Type', 'Mode', 'Client Count', 'Latency', 'Throughput', 'CPU Usage',
                                     'Memory Usage'])
    df.to_csv(os.path.join(metric_results_dir, 'metric_results.csv'), index=False)


def main(client_counts, modes, client_files_path, metric_plots_path, metric_results_path,
         tornado_hostname, websockets_hostname):
    """
    Main function to run the tests, plot metrics, and save results.
    """
    all_results = {}
    print("Tornado Test Start ...")

    results_1 = tornado_test(hostname=tornado_hostname, all_mode=modes, client_files_dir=client_files_path,
                             client_count=client_counts)
    print("###################################################################")
    print("Websockets Test Start ...")
    results_2 = asyncio.run(websockets_test(hostname=websockets_hostname, all_mode=modes,
                                            client_files_dir=client_files_path, client_count=client_counts))

    all_results.update(results_1)
    all_results.update(results_2)

    plot_and_save_metric(all_results, 0, 'Average Latency', 'Latency (s)', metric_plots_path)
    plot_and_save_metric(all_results, 1, 'Throughput', 'Messages/Second per Client', metric_plots_path)
    plot_and_save_metric(all_results, 2, 'CPU Usage', 'CPU Usage (%)', metric_plots_path)
    plot_and_save_metric(all_results, 3, 'Memory Usage', 'Memory Usage (%)', metric_plots_path)

    save_final_bar_chart(all_results, metric_plots_path)
    save_final_results(all_results, metric_results_path)
    print("Test is finished ... ")
    print("The results is saved under metric_plots folder ... ")
    print("Stopping WebSocket server ... ")
    try:
        asyncio.run(websockets_test(hostname=websockets_hostname, all_mode=modes,
                                    client_files_dir=client_files_path, client_count=client_counts, close_flag=True))
    except Exception as e:
        print(f"Error: {e}")
    print("Stopping Tornado server ... ")
    tornado_test(hostname=tornado_hostname, all_mode=modes, client_files_dir=client_files_path,
                 client_count=client_counts, close_flag=True)
    print("All Servers are closed ... ")
    exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run WebSocket performance tests.')

    parser.add_argument('--client_counts', nargs='+', type=int,
                        default=[1, 10, 20, 50], help='List of client counts to test.')
    parser.add_argument('--modes', nargs='+', default=['data', 'file'], help='Modes to test.')
    parser.add_argument('--client_files_path', type=str, default="client-files/", help='Path for client files.')
    parser.add_argument('--metric_plots_path', type=str, default="metric_plots/", help='Path for metric plots.')
    parser.add_argument('--metric_results_path', type=str, default="metric_results/", help='Path for metric results.')
    parser.add_argument('--tornado_hostname', type=str, default="tornado_server:8765",
                        help='Address for Tornado server.')
    parser.add_argument('--websockets_hostname', type=str,
                        default="websockets_server:8765", help='Address for Websockets server.')

    args = parser.parse_args()

    main(args.client_counts, args.modes, args.client_files_path, args.metric_plots_path,
         args.metric_results_path, args.tornado_hostname, args.websockets_hostname)
