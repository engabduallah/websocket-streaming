"""
Author: Abdullah Damash
Date: 15/12/2023
Copyright (c) 2023, A.Damash. All rights reserved.

This script establishes a Tornado server and WebSocket for handling real-time
communications. It's structured to follow PEP 8 guidelines for Python code.
"""

import json
import tornado.ioloop
import tornado.web
import tornado.websocket
import argparse

from message_handler import MessageHandler


class TornadoStartSocket:
    def __init__(self, port, server_files_dir, hostname):
        """
        Initialize the TornadoStartSocket with specified port and directory for server files.
        """
        self.app = None
        self.hostname = hostname
        self.port = port
        self.server_files_dir = server_files_dir

    def start_tornado_server(self):
        """
        Start the Tornado server. It listens on the specified port and initializes
        the WebSocket handler.
        """
        self.app = tornado.web.Application([
            (r"/", TornadoWebSocket, {'server_files_dir': self.server_files_dir})
        ])
        self.app.listen(self.port, address=self.hostname)
        tornado.ioloop.IOLoop.current().start()

    @staticmethod
    def stop_tornado_server():
        tornado.ioloop.IOLoop.current().stop()
        print("server has been closed")


class TornadoWebSocket(tornado.websocket.WebSocketHandler):
    def initialize(self, server_files_dir):
        """
        This method is provided by Tornado to initialize additional parameters.
        It's called with the arguments from the URLSpec/Route that maps to this handler.
        """
        self.server_files_dir = server_files_dir

    def open(self):
        """
        Handle a new WebSocket connection. This method is called when a new
        client connection is established.
        """
        self.write_message(json.dumps({"message": "Client connected"}))

    def on_close(self):
        """
        Handle the closing of a WebSocket connection. You can add custom code here
        for cleanup or logging when a client disconnects.
        """
        pass

    async def on_message(self, message):
        """
        Asynchronously handle incoming messages on the WebSocket. This method is
        called when a new message is received from the client.
        """
        try:
            # Parse the JSON message received from the client
            parsed_message = json.loads(message)
            if parsed_message.get("mode") == "close connection":
                print("Close command received. Closing connection.")
                TornadoStartSocket.stop_tornado_server()
            # Initialize the MessageHandler with the parsed message and callback functions
            handler = MessageHandler(parsed_message, self.write_message, self.server_files_dir)

            # Respond to the message
            await handler.respond()
        except Exception as e:
            # Print an error message if message handling fails
            print(f"Error handling message: {e}")


if __name__ == "__main__":
    # Create the parser
    parser = argparse.ArgumentParser(description='Start the Tornado server with specified port and directory.')

    # Define the expected command-line arguments
    parser.add_argument('--hostname', type=str, help='hostname of the server.')
    parser.add_argument('--port', type=int, help='Port number for the Tornado server to listen on.')
    parser.add_argument('--server_files_dir', type=str, help='Directory path for the server files.')

    # Parse arguments
    args = parser.parse_args()

    # If this script is run as the main program, start the Tornado server
    tornado_server = TornadoStartSocket(args.port, args.server_files_dir, args.hostname)
    tornado_server.start_tornado_server()
