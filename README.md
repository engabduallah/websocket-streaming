# WebSocket Streaming System
Tornado Vs. websockets

## Authors

- [@engabduallah](https://gitlab.com/engabduallah)

## Overview

### Tornado Features:

Tornado: Known for its excellent performance and scalability, Tornado is an asynchronous networking library that can handle long-lived network connections. It's well-suited for real-time web applications.

* Can handle up to 10,000 simultaneous open connections, making it ideal for applications with high levels of concurrent users.
* Can handle multiple requests simultaneously without blocking requests.
* One of the few web frameworks that supports WebSocket connections.

Reference:  https://github.com/tornadoweb/tornado

### Websockets Features:

Websockets: A library dedicated to providing WebSocket communication. It’s easy to use and supports both synchronous and asynchronous programming. This library is ideal for real-time data exchange over WebSockets.

* Data can be sent and received quickly, allowing for real-time communication.
* Enable bidirectional communication between the client and the server.
* Use the secure websocket protocol (WSS) which encrypts all data sent over the connection. 

Reference:  https://github.com/python-websockets/websockets
## Getting started

First Step, create a virtual environment:
```bash
python -m venv venv 
```
Then, in your terminal, you need to activate virtual environment by running the following command: 
- For PowerShell Terminal:
```powershell
venv\Scripts\Activate.ps1
```
- For CMD Terminal:
```cmd
venv\Scripts\activate.bat
```
- For Bash Terminal:
```bash
source  venv/Scripts/activate
```

## Running the Setup
Run tornado_server.py in one terminal to start the Tornado server.
```bash
python websockets_server.py --hostname "localhost" --port 8766 --server_files_dir "server-files"
```
Run websockets_server.py in a second terminal to start the WebSocket server.
```bash
python tornado_server.py --hostname "localhost" --port 8765 --server_files_dir "server-files"
```
Run clients-analysis.py in a third terminal to start the client, which will connect to one of the servers and begin the message exchange process.
```bash
python clients-analysis.py --client_counts 1 10 20 50 --modes data file --client_files_path  "client-files/" --metric_plots_path "metric_plots/" --metric_results_path "metric_results/" --tornado_hostname "localhost:8765" --websockets_hostname "localhost:8765"
```

## Test Environment
**There are two modes for Data Type:** 

*  **File Mode**: this represents the mode of sending file chunks to the client between server and client. 

*  **Data Mode**: this represents the mode of sending JSON-formatted message between server and client.

**Data Size Streamed:** 

each approximately 1KB in size.


**Hardware Details**:
* CPU: Intel Core i7
* RAM: 64GB
* Operating System: Linux (Ubuntu 20.04)
* Cores and Threads: Single core, single thread utilized.

### Notes on Collected Metrix: 

**Latency**: Calculated as the time difference between sending a message and receiving a response.

**Throughput**: Calculated as the number of messages sent and received per unit of time.

**CPU and Memory Usage**: Measured using the psutil library. These values reflect the usage of the entire system, not just the Python process, so for realistic testing, I used Docker containers in which, I create for each server different container and run them as docker compose. 

You can run the test scrip by following command:
```bash
 docker-compose up --build
```

## Compression Results
For the compression results, please check the folders **metric_plots** and **metric_results**.

### Tornado Server Analysis

#### Data Mode

- **Latency**: Increases with the number of clients; highest at 50 clients.
- **Throughput**: High for a single client; decreases significantly as the client count increases.
- **CPU Usage**: Peaks at 10 clients and slightly decreases thereafter, suggesting a possible resource bottleneck.
- **Memory Usage**: Remains stable regardless of the client count.

#### File Mode

- **Latency**: Lower compared to data mode, with a gradual increase as client count increases.
- **Throughput**: Best with one client; decreases with more clients but not as sharply as in data mode.
- **CPU Usage**: Less variation compared to data mode; relatively stable across different client counts.
- **Memory Usage**: Consistently stable, similar to data mode.

### WebSockets Server Analysis

#### Data Mode

- **Latency**: Slightly higher for a single client compared to Tornado, with a consistent upward trend as client count increases.
- **Throughput**: Decreases as the number of clients increases but maintains a more gradual decline, indicating better scalability.
- **CPU Usage**: Generally lower than Tornado's, indicating more efficient resource utilization.
- **Memory Usage**: Stable across varying client counts.

#### File Mode

- **Latency**: Starts higher than data mode and increases with client count.
- **Throughput**: Reduced compared to data mode but shows similar scalability to Tornado.
- **CPU Usage**: Noticeably lower than Tornado's, especially at higher client counts.
- **Memory Usage**: Consistent and comparable to data mode.

### General Insights

- **Scalability**: WebSockets server demonstrates better scalability in handling more clients in both data and file modes.
- **Resource Efficiency**: WebSockets server shows efficient CPU usage, which is particularly evident in file mode.
- **Stability**: Both servers showcase stable memory usage, indicating good memory management.
- **Mode Comparison**: Tornado handles file mode slightly better than data mode in terms of throughput under load.

### Recommendations

- Conduct profiling under load for Tornado server to identify optimization opportunities.
- Focus on optimizations for concurrent operations, especially for Tornado server.
- Consider the WebSockets server for applications expecting many simultaneous connections due to its better performance at scale.
- Evaluate the server choice based on the specific use case, considering the strengths of each server in handling data and file modes.


**It's important to note that these insights are based on the data provided and actual application performance might vary based on numerous factors including network conditions, hardware specifications, and specific workload characteristics.**

### **Latest Plots**: 
### Overall Compression
![img.png](metric_plots/final_results_bar_chart.png)

### Tornado Server Plots - Data Mode
<img src="metric_plots/Tornado_data_average_latency.png" width="250" /> 
<img src="metric_plots/Tornado_data_cpu_usage.png" width="250" />
<img src="metric_plots/Tornado_data_memory_usage.png" width="250" /> 
<img src="metric_plots/Tornado_data_throughput.png" width="250" />

### Tornado Server Plots - File Mode
<img src="metric_plots/Tornado_file_average_latency.png" width="250" /> 
<img src="metric_plots/Tornado_file_cpu_usage.png" width="250" />
<img src="metric_plots/Tornado_file_memory_usage.png" width="250" /> 
<img src="metric_plots/Tornado_file_throughput.png" width="250" />

### WebSockets Server Plots - Data Mode
<img src="metric_plots/websockets_data_average_latency.png" width="250" /> 
<img src="metric_plots/websockets_data_cpu_usage.png" width="250" />
<img src="metric_plots/websockets_data_memory_usage.png" width="250" /> 
<img src="metric_plots/websockets_data_throughput.png" width="250" />

### WebSockets Server Plots - File Mode
<img src="metric_plots/websockets_file_average_latency.png" width="250" /> 
<img src="metric_plots/websockets_file_cpu_usage.png" width="250" />
<img src="metric_plots/websockets_file_memory_usage.png" width="250" /> 
<img src="metric_plots/websockets_file_throughput.png" width="250" />