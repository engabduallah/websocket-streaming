"""
Author: Abdullah Damash
Date: 15/12/2023
Copyright (c) 2023, A.Damash. All rights reserved.

This code is written by A. Damash following the honor codes.
"""

import json
import os


class MessageHandler:
    def __init__(self, message, write_function, server_files_dir):
        """
        Initialize the MessageHandler with a message, a function to write responses,
        and a directory path for server files.
        """
        self.message = message
        self.write = write_function
        self.server_files_dir = server_files_dir

    async def respond(self):
        """
        Respond to the message based on its mode. If the mode is 'file', it sends files.
        If the mode is 'data', it sends a JSON-formatted message.
        """
        mode = self.message.get("mode")
        if mode == "file":
            await self._send_files(f"{self.server_files_dir}/")
        elif mode == "data":
            await self._send_data()

    async def _send_files(self, directory_path):
        """
        Send files from the specified directory. This method lists all files in the directory
        and sends each one.
        """
        if os.path.exists(directory_path):
            for filename in os.listdir(directory_path):
                file_path = os.path.join(directory_path, filename)
                if os.path.isfile(file_path):
                    try:
                        await self._send_file(file_path)
                    except Exception as e:
                        print(f"Error sending file {file_path}: {e}")
        else:
            print(f"Directory {directory_path} does not exist.")

    async def _send_file(self, file_path):
        """
        Send a file in chunks. This method reads the file in binary mode and sends it in chunks.
        """
        with open(file_path, 'rb') as file:
            while True:
                chunk = file.read(8192)  # Read the file in chunks of 8192 bytes
                if not chunk:
                    break
                await self.write(chunk, binary=True)

    async def _send_data(self):
        """
        Send a JSON-formatted message. This method sends a simple JSON message.
        """
        await self.write(json.dumps({"message": "Hello, this is data in JSON format"}))
